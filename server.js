const express = require('express');
const bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient

const app = express();
// add body parser so we can parse POST body data
app.use(bodyParser.urlencoded({extended: true}));
// teach our server to speak JSON
app.use(bodyParser.json())
// set the view engine
app.set('view engine', 'ejs');
// make public folder "publicly" available
app.use(express.static('public'))

const dbUrl = 'mongodb://vincent:yoda123@ds135830.mlab.com:35830/node-crud-starwars-app';
let db;

MongoClient.connect(dbUrl, { useNewUrlParser: true }, (err, client) => {
  if (err) return console.log(err);
  db = client.db('node-crud-starwars-app');
  app.listen(3000, () => {
    console.log('listening on 3000...')
  });
});

app.get('/', (req, res) => {
  const cursor = db.collection('quotes').find();
  cursor.toArray((err, results) => {
    console.log(results);
    // send html file with populated results...
    res.render('index.ejs', { quotes: results });
  })
  // res.sendFile(__dirname + '/index.html');
});

app.post('/quotes', (req, res) => {
  // console.log(req.body);
  db.collection('quotes').save(req.body, (err, result) => {
    if (err) return console.log(err);
    console.log('saved to database');
    res.redirect('/');
  })
});

app.put('/quotes', (req, res) => {
  // Handle put request
  db.collection('quotes').findOneAndUpdate(
    {
      name: 'Yoda'
    },
    {
      $set: {
        name: req.body.name,
        quote: req.body.quote
      }
    },
    {
      sort: { _id: -1 },
      upsert: true
    },
    (err, result) => {
      if (err) return console.log(err);
      res.send(result);
    }
  )  
});

app.delete('/quotes', (req, res) => {
  // Handle delete event here
  db.collection('quotes').findOneAndDelete(
    {
      name: req.body.name
    },
    (err, result) => {
      if (err) return res.send(500, err)
        res.send({message: 'A darth vadar quote got deleted'})
    }
  )
})

// console.log('may the force be with you');